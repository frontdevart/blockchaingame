import { useState, useEffect, useCallback } from 'react'
import { CSSTransition, Transition, TransitionGroup } from 'react-transition-group';
import './App.css';

const currencies = ['BTC', 'ETC', 'ADA', 'SOL', 'BNB', 'XRP', 'DOGE', 'DOT'];
const values = [0.1, 1, 5, 10];

const App = () => {
  const [cryptoArr, setCryptoArr] = useState([]);
  const [winings, setWinings] = useState({});
  const [showElem, setShowElem] = useState(false);
  const [pending, setPending] = useState(false);
  const [convertPending, setConvertPending] = useState(false);
  const [hideWiningList, setHideWiningList] = useState(true);

  useEffect(() => {
    if(showElem) {
      setTimeout(() => {
        setShowElem(false);
        setPending(false);
      }, 1000);
    }
  }, [showElem]);

  const setWiningFunc = useCallback(() => {
    const lastCrypto = cryptoArr[cryptoArr.length - 1];
    setWinings(prev => ({
      ...prev,
      [lastCrypto.currency]: {
        currency: lastCrypto.currency,
        value: cryptoArr
                .filter(item => item.currency === lastCrypto.currency)
                .reduce((a, b) => a + b.value, 0)
      }
    }));
  }, [cryptoArr]);

  useEffect(() => {
    if(cryptoArr.length) {
      setWiningFunc();
    }
  }, [cryptoArr, setWiningFunc]);

  const getWin = () => {
    const randIdxCurr = Math.floor(Math.random() * currencies.length);
    const randIdxVal = Math.floor(Math.random() * values.length);
    return { currency: currencies[randIdxCurr], value: values[randIdxVal] };
  }

  const clickHandler = () => {
    setCryptoArr(prev => ([...prev, getWin()]));
    setShowElem(true);
    setPending(true);
    setHideWiningList(false);
  }

  const deleteSItem = param => {
    setWinings(prevState => {
      const state = { ...prevState };
      delete state[param];
      return state;
    });
  };

  const convertHandler = () => {
    const items = Object.keys(winings).reverse();
    let i = 0;
    const interval = setInterval(() => {
      deleteSItem(items[i]);
      setConvertPending(true);
      i += 1;
      if (i >= items.length + 1) {
        setHideWiningList(true);
        setConvertPending(false);
        setCryptoArr([]);
        clearInterval(interval);
      }
    }, 500);
  }

  return (
    <div className="App">
        <div className="button-crypto-item-wrapper">  
          <button className="btn" disabled={pending || convertPending} onClick={clickHandler}>Bet $10</button>
            <Transition
              in={showElem}
              timeout={10000}
              mountOnEnter
              unmountOnExit
            >
              {state => (
                <li className={`crypto-item ${state}`}>
                  <img className="crypto-item-img" src={`/images/${cryptoArr[cryptoArr.length - 1]?.currency}.png`} alt={cryptoArr[cryptoArr.length - 1]?.currency} />
                  <span>{cryptoArr[cryptoArr.length - 1]?.value}</span>
                </li>
              )}
            </Transition>
          <button disabled={convertPending} className="btn convert" onClick={convertHandler}>Convert All To $</button>
        </div>
        {!hideWiningList && (
          <TransitionGroup component="div" className="winings-list">
              {Object.keys(winings).map((item, i) => (
                <CSSTransition
                  key={i}
                  timeout={500}
                  classNames="wining-item"
                >
                    {winings[item] && <div className="wining-item">
                      <div className="img-wrapper">
                        <img className="crypto-item-img" src={`/images/${winings[item]?.currency}.png`} alt={winings[item]?.currency} />
                      </div>
                      <span className={pending && item === cryptoArr[cryptoArr.length - 1].currency ? 'scale' : ''}>{winings[item]?.value?.toFixed(1)}</span>
                  </div>}
                </CSSTransition>
              ))}
          </TransitionGroup>
          )}
    </div>
  )
}

export default App
